import { userConstants } from '../constants'

export function users (state = {}, action) {
  console.log(state)
  console.log(action.payload)
  switch (action.type) {
    case userConstants.GETALL_REQUEST:
      return {
        loading: true
      }
    case userConstants.LOAD:
      return {
        items: action.payload
      }
    case userConstants.GETALL_FAILURE:
      return {
        error: action.message
      }
    default:
      return state
  }
}
