import { combineReducers } from 'redux'

import { users } from './users.reducer'
import { reducer as reduxFormReducer } from 'redux-form'
const rootReducer = combineReducers({
  form: reduxFormReducer,
  users: users
})

export default rootReducer
