import React from 'react'
import { Wrapper, TitleText } from './styles'

export const Title = ({ title = 'BotXO' }) => (
  <Wrapper>
    <TitleText>{title}</TitleText>
  </Wrapper>
)
