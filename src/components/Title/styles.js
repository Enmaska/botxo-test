import styled from 'styled-components'

export const Wrapper = styled.div`
  display: flex;
  overflow: scroll;
  width: 100%;
`

export const TitleText = styled.h1`
  font-size: 24px;
`
