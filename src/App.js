import React from 'react'
import GetAndEditData from './pages/GetAndEditData'
import { GlobalStyle } from './GlobalStyles'

export const App = () => (
  <div>
    <GlobalStyle />
    <GetAndEditData />
  </div>
)
