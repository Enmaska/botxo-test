import React, { useState, useEffect } from 'react';
import axios from 'axios';

function Tapas () {
  const [data, setData] = useState({ tapas: [] })

  useEffect(() => {
    const getTapas = async () => {
      const result = await axios(
        'https://rutadelatun.adrianrueda.dev/json/tapas.json'
      )
      setData(result.data.data)
      //   console.log(result.data.data.tapas);
    };
    getTapas()
  }, [])

  return (
    <div className='row'>
      {/* {console.log(data.tapas)} */}
      {data.tapas.map(item => (
        <div className='col-sm-4 col-jurado mbb' key={item.id}>
          <a
            href={item.medium_cover}
            data-lightbox='ruta'
            data-title={item.title}
          >
            <img src={item.medium_cover_image} className='w100' />
          </a>
          <h4>{item.restaurante} </h4>
          <p>{item.title}</p>
        </div>

        // <li key={item.id}>
        //   <img src={item.medium_cover_image} alt="" />
        //   {item.restaurante} {item.title}
        // </li>
      ))}
    </div>
  )
}

// function Tapas() {
//   const [data, setData] = useState({ data: [] });

//   useEffect(() => {
//     const getPhotos = async () => {
//       const result = await axios("https://jsonplaceholder.typicode.com/photos");
//       setData(result);
//     };
//     getPhotos();
//   }, []);

//   return (
//     <ul>
//       {data.data.map(item => (
//         <li key={item.id}>
//           <a href={item.url}>{item.title}</a>
//         </li>
//       ))}
//     </ul>
//   );
// }

export default Tapas
