import React, { useState, useEffect } from 'react'
import { Field, reduxForm, FieldArray } from 'redux-form'
import { connect } from 'react-redux'
import { Title } from '../../components/Title'
import loadAccount from '../../actions/users'
// import Tapas from '../../hooks/test'
import { useFetch } from '../../hooks/useFetch'
const onSubmit = (event) => {
  event.preventDefault()
  console.log('hola que pasa')
}

const renderField = ({ input, label, type, meta: { touched, error } }) => (
  <div>
    <label>{label}</label>
    <div>
      <input {...input} type={type} placeholder={label} />
      {touched && error && <span>{error}</span>}
    </div>
  </div>
)

const renderUsers = ({ fields, meta: { error, submitFailed } }) => (
  <ul>
    <li>
      <button type='button' onClick={() => fields.push({})}>
          Add User
      </button>
      {submitFailed && error && <span>{error}</span>}
    </li>
    {fields.map((user, index) => (
      <li key={index}>
        <button
          type='button'
          title='Remove User'
          onClick={() => fields.remove(index)}
        />
        <h4>User #{index + 1}</h4>
        <Field
          name={`${user}.name`}
          type='text'
          component={renderField}
          label='Name'
        />
        <Field
          name={`${user}.age`}
          type='number'
          component={renderField}
          label='Age'
        />
      </li>
    ))}
  </ul>
)

function useCategoriesData () {
  const [categories, setCategories] = useState([])
  const [loading, setLoading] = useState(false)

  useEffect(async function () {
    setLoading(true)
    await window.fetch('http://webhook.site/27d8a615-2f74-4220-b0c3-2dfc5a1bed02')
      .then(res => res.json())
      .then(response => {
        setCategories(response)
        setLoading(false)
      })
  }, [])
  console.log(categories)
  console.log(loading)
  return { categories, loading }
}

let GetAndEditData = props => {
  const { pristine, reset, submitting, handleSubmit, load } = props
  const { categories, loading } = useCategoriesData()
  return (
    <div>

      <Title title='Generic Data' />
      <form onSubmit={handleSubmit(onSubmit)}>
        <div>
          <button type='button' onClick={() => load()}>Load Account</button>
        </div>
        <FieldArray name='generic_data' component={renderUsers} />
        <div>
          <button type='submit' disabled={submitting}>
          Submit
          </button>
          <button type='button' disabled={pristine || submitting} onClick={reset}>
          Clear Values
          </button>
        </div>
      </form>
    </div>

  )
}

GetAndEditData = reduxForm({
  form: 'initializeFromState', // a unique identifier for this form
  enableReinitialize: true,
  initialValues: {
    generic_data: [{ name: 'paco', age: '12' }]
  }
})(GetAndEditData)

// You have to connect() to any reducers that you wish to connect to yourself
GetAndEditData = connect(
  state => ({
    initialValues: {
      generic_data: state.users.items
    } // pull initial values from account reducer
  }),
  { load: loadAccount } // bind account loading action creator
)(GetAndEditData)

export default GetAndEditData

// export default reduxForm({
//   form: 'simple',
//   enableReinitialize: true,
//   initialValues: {
//     generic_data: [{ name: '', age: '' }]
//   }
// })(GetAndEditData)
